/**
 * Class to hold the keys for the local storage, to avoid magic strings.
 */
export class SettingsKeys {
  public static TEXT_LENGTH = 'textLength';

  public static LAST_BOOK_URN = 'lastBookUrn';
  public static LAST_AUTHOR = 'lastAuthor';

  public static THEME = 'theme';
  public static FONT_SIZE = 'fontSize';
  public static FONT_FAMILY = 'fontFamily';
  public static LINE_HEIGHT = 'lineHeight';
  public static MARGIN_WIDTH = 'marginWidth';
}
