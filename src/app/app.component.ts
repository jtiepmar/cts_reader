import {Component} from "@angular/core";
import {Platform} from "ionic-angular";
import {StatusBar, Splashscreen} from "ionic-native";
import {ReaderPage} from "../pages/reader/reader";
import {SettingsService} from "../services/settings.service";

@Component({
  template: `<div [class]="chosenTheme">
      <ion-nav [root]="rootPage"></ion-nav>
    </div>`
})
export class MyApp {
  rootPage = ReaderPage;

  /**
   * The currently set theme. Is used in the template of the page to assign it.
   */
  chosenTheme: string;

  /**
   * Retrieves the theme from the SettingService by subscribing to any changes of it.
   * After that it starts the application.
   * @param platform
   * @param settings
   */
  constructor(platform: Platform, private settings: SettingsService) {

    this.settings.getTheme()
      .subscribe(val => {
        this.chosenTheme = val;
        console.log('new theme: ' + this.chosenTheme);
      });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }
}
