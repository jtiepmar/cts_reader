import {NgModule} from "@angular/core";
import {IonicApp, IonicModule} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {MyApp} from "./app.component";
import {ReaderPage} from "../pages/reader/reader";
import {AboutPage} from "../pages/about/about";
import {PrintPage} from "../pages/print/print";
import {GeneralSettingsPage} from "../pages/generalSettings/generalSettings";
import {LibraryAuthorPage} from "../pages/library/library-author";
import {LibraryBooksPage} from "../pages/library/library-books";
import {BookService} from "../services/book.service";
import {ReaderService} from "../services/reader.service";
import {AuthorService} from "../services/author.service";
import {DesignPage} from "../pages/design/design";
import {SearchPage} from "../pages/search/search";
import {ReportBrokenPage} from "../pages/reportBroken/reportBroken";
import {SettingsService} from "../services/settings.service";
import {BaseUrlPage} from "../pages/baseUrl/baseUrl";

@NgModule({
  declarations: [
    MyApp,
    ReaderPage,
    AboutPage,
    LibraryAuthorPage,
    LibraryBooksPage,
    GeneralSettingsPage,
    BaseUrlPage,
    DesignPage,
    SearchPage,
    ReportBrokenPage,
    PrintPage
  ],

  /**
   * Here all components with there possible navigation parameters and default history get declared.
   */
  imports: [
    IonicModule.forRoot(MyApp, {}, {
      links: [
        {component: ReaderPage, name: 'Reader', segment: 'reader/:baseUrl'},
        {component: ReaderPage, name: 'Reader', segment: 'reader/:baseUrl/:bookUrn'},

        {component: LibraryAuthorPage, name: 'Library', segment: 'library/:baseUrl', defaultHistory: [ReaderPage]},
        {
          component: LibraryBooksPage,
          name: 'Library', segment: 'library/:baseUrl/:authorName',
          defaultHistory: [ReaderPage, LibraryAuthorPage]
        },

        {component: AboutPage, name: 'About', segment: 'about', defaultHistory: [ReaderPage]},
        {
          component: ReportBrokenPage,
          name: 'Report broken corpus', segment: 'reportBroken',
          defaultHistory: [ReaderPage]
        },
        {component: SearchPage, name: 'Search', segment: 'search/:baseUrl', defaultHistory: [ReaderPage]},
      ]
    })
  ],

  bootstrap: [IonicApp],

  entryComponents: [
    MyApp,
    ReaderPage,
    AboutPage,
    ReportBrokenPage,
    LibraryAuthorPage,
    LibraryBooksPage,
    GeneralSettingsPage,
    BaseUrlPage,
    DesignPage,
    SearchPage,
    PrintPage
  ],

  /**
   * Defines the services so they can be used and are only used as a singleton.
   */
  providers: [
    Storage,
    AuthorService,
    BookService,
    ReaderService,
    SettingsService
  ]
})

export class AppModule {
}
