import {OnInit, Component} from "@angular/core";
import {ViewController, NavParams} from "ionic-angular";
import {SettingsService} from "../../services/settings.service";

/**
 * Class for the Design component.
 * It allows to set various styling settings, like font size,
 * line height, font family and margin width.
 */
@Component({
  selector: 'page-design',
  templateUrl: 'design.html',
})
export class DesignPage implements OnInit {

  selectedTheme: String;
  availableThemes: {className: string, prettyName: string}[];

  fontSize: string;
  fontFamily: string;
  lineHeight: string;
  marginWidth: string;

  constructor(private viewCtrl: ViewController,
              private params: NavParams,
              private settings: SettingsService) {

    this.settings.getTheme().subscribe(val => this.selectedTheme = val);
    this.availableThemes = this.settings.availableThemes;
  }

  ngOnInit(): void {
    this.fontSize = this.params.get('fontSize');
    this.fontFamily = this.params.get('fontFamily');
    this.lineHeight = this.params.get('lineHeight');
    this.marginWidth = this.params.get('marginWidth');
  }

  dismiss() {
    this.viewCtrl.dismiss({
      fontSize: this.fontSize,
      fontFamily: this.fontFamily,
      lineHeight: this.lineHeight,
      marginWidth: this.marginWidth
    });
  }

  /**
   * Passes the set theme to the SettingsService.
   * @param e
   */
  public setTheme(e) {
    this.settings.setTheme(e);
  }
}
