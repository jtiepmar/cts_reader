import {OnInit, Component} from "@angular/core";
import {ViewController, NavParams} from "ionic-angular";
import {ReaderService} from "../../services/reader.service";

/**
 * Declares the pdfMake library to prevent errors from the
 * typescript compiler, as there are no current typings available.
 */
declare let pdfMake: any;

/**
 * Class for the PrintPage component.
 */
@Component({
  selector: 'page-print',
  templateUrl: 'print.html',
})
export class PrintPage implements OnInit {

  docDefinition;

  licence: string;
  urn: string;
  title: string;
  author: string;

  printFontSize: string = '12';

  constructor(private viewCtrl: ViewController,
              private params: NavParams,
              private readerService: ReaderService) {}

  ngOnInit() {
    this.licence = this.params.get('licence');
    this.urn = this.params.get('urn');
    this.title = this.params.get('title');
    this.author = this.params.get('author');
    this.printFontSize = this.params.get('printfontSize');
    this.callBackPrintPage();
  }

  dismiss() {
    this.viewCtrl.dismiss({});
  }

  /**
   * Creates the content for the pdf/text document by following the pdfmake library guidelines.
   */
  callBackPrintPage() {
    this.readerService.getPassage(this.urn, this.params.get('baseUrl'))
      .subscribe(urnContent => {
        this.docDefinition = {
          content: [{
            text: ` 
              ${this.urn}        
              ${this.author}
              ${this.title}
              
              ${this.licence}
               
              ${urnContent}`,
            style: 'userPreference' }],
          styles: {
            userPreference: {
              fontSize: this.printFontSize
            }
        }};
      });
  }

  /**
   * Creates a prompt for the user to download the pdf file using the pdfmake library.
   */
  downloadPDF() {
    pdfMake.createPdf(this.docDefinition).download(`ctsReader_excerpt.pdf`);
  }

  /**
   * Opens the pdf file in a new tab while using the pdfmake library.
   */
  openPDF() {
    pdfMake.createPdf(this.docDefinition).open();
  }

  /**
   * Creates a prompt for the user to download the plain text file.
   */
  downloadText() {
    this.readerService.getPassage(this.urn, this.params.get('baseUrl'))
      .subscribe(urnContent => {
          let content:string =
          `${this.urn}        
          ${this.author}
          ${this.title}
          ${this.licence}
          
          
          ${urnContent}`;

        this.download('ctstext.txt', content);
      });

  }

  /**
   * Creates the plain text file.
   * @param filename The name of the file
   * @param text The contents of the file
   */
  private download(filename:string, text:string) {
    let element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
}
