import {Component, OnInit} from "@angular/core";
import {ViewController, NavParams} from "ionic-angular";

/**
 * Class for the ReportBroken component.
 */
@Component({
  selector: 'page-reportBroken',
  templateUrl: 'reportBroken.html'
})
export class ReportBrokenPage implements OnInit {

  author: string = '';
  title: string = '';
  currentUrn: string = '';
  mailAddress: string = 'jtiepmar@informatik.uni-leipzig.de';
  mailSubject: string = '[BROKENCTSDOCUMENT]';
  mailBody: string = '';
  newLine: string = '%0D%0A';
  remarks: string = '';
  usedUrl: string = '';

  constructor(params: NavParams,
              public viewCtrl: ViewController) {
    this.author = params.get('author');
    this.title = params.get('title');
    this.currentUrn = params.get('urn');
    this.usedUrl = params.get('url');
  }

  ngOnInit() {}

  dismiss() {
    this.viewCtrl.dismiss();
  }

  /**
   * Opens the registered mail application on the user system with
   * a predefined message.
   */
  mailTo() {
    if (this.remarks == '') {
      this.remarks = `${this.newLine}No additional user comments entered.`;
    } else {
      this.remarks = `${this.newLine}Additional remarks by the user: ${this.remarks}`;
    }

    this.mailSubject += ' ' + this.title;
    this.mailBody = `Broken document: ${this.title}${this.newLine} 
      Author: ${this.author}${this.newLine}
      Full URL used: ${this.usedUrl}${this.newLine}
      ${this.remarks}`;

    window.location.href = `mailto:${this.mailAddress}?subject=${this.mailSubject}
      &body=${this.mailBody}`;
    this.dismiss();
  }
}
