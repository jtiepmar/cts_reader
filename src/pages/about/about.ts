import {Component} from "@angular/core";
import {Storage} from "@ionic/storage";
import {ViewController, ModalController, NavParams} from "ionic-angular";
import {ReportBrokenPage} from "../reportBroken/reportBroken";
import {SettingsKeys} from "../../util/settingskeys";

/**
 * Class for the About component.
 */
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public viewCtrl: ViewController,
              public modalCtrl: ModalController,
              public params: NavParams,
              public storage: Storage) {
  }

  ngOnInit() {
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  /**
   * Function to open the ReportBroken component with the necessary navigation parameters.
   * @param myEvent
   */
  openReportBroken(myEvent) {
    this.storage.get(SettingsKeys.LAST_AUTHOR).then((author: string) => {
      let reportBrokenModal = this.modalCtrl.create(ReportBrokenPage, {
        author: author,
        url: window.location.href,
        urn: this.params.get('urn'),
        title: this.params.get('title')
      });
      reportBrokenModal.present();
    });
  }

}
