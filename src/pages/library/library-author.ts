import {Component, OnInit} from "@angular/core";
import {NavController, AlertController, NavParams} from "ionic-angular";
import {AuthorService} from "../../services/author.service";
import {BookService} from "../../services/book.service";
import {Author} from "../../model/author";
import {LibraryBooksPage} from "./library-books";
import {SettingsKeys} from "../../util/settingskeys";
import {Storage} from "@ionic/storage";

/**
 * Class for the author page in the library.
 */
@Component({
  selector: 'page-library-author',
  templateUrl: 'library-author.html',
  providers: [AuthorService, BookService]
})
export class LibraryAuthorPage implements OnInit {

  authors: Array<Author>;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private alertCtrl: AlertController,
              private authorService: AuthorService,
              private storage: Storage) {
  }

  /**
   * Checks if a last author is defined in local storage and
   * opens the page with its books.
   * Otherwise it fetches all available authors from cts server.
   */
  ngOnInit() {
    this.authorService.getAuthors(this.navParams.get("baseUrl"))
      .subscribe((authors) => {
        this.authors = authors;
      }, (error) => {
        let alert = this.alertCtrl.create({
          title: 'Error!',
          subTitle: 'There was an error retrieving the list of authors. '
          + 'Please check your connection and settings.',
          buttons: ['OK']
        });
        alert.present();
      });
  }

  /**
   * Opens the library page with books of the selected author.
   * @param author
   */
  goToBooksPage(author: Author) {
    this.storage.set(SettingsKeys.LAST_AUTHOR, author.name);
    this.navCtrl.push(LibraryBooksPage, {
      authorName: author.name,
      baseUrl: this.navParams.get("baseUrl")
    });
  }
}




