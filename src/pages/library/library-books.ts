import {Component, OnInit} from "@angular/core";
import {NavController, AlertController, NavParams} from "ionic-angular";
import {AuthorService} from "../../services/author.service";
import {BookService} from "../../services/book.service";
import {Author} from "../../model/author";
import {Book} from "../../model/book";
import {ReaderPage} from "../reader/reader";

/**
 * Class for the books page in the library.
 */
@Component({
  selector: 'page-library-books',
  templateUrl: 'library-books.html',
  providers: [AuthorService, BookService]
})
export class LibraryBooksPage implements OnInit {

  selectedAuthor: Author;
  books: Array<Book>;

  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              private navParams: NavParams,
              private bookService: BookService) {
  }

  /**
   * Fetches all books of the provided author.
   */
  ngOnInit() {
    this.selectedAuthor = new Author();
    this.selectedAuthor.name = this.navParams.get('authorName');

    this.bookService.getBooks(this.selectedAuthor, this.navParams.get("baseUrl"))
      .subscribe(books => {
        this.books = books

      }, (error) => {
        let alert = this.alertCtrl.create({
          title: 'Error!',
          subTitle: 'There was an error retrieving the list of books. '
          + 'Please check your connection and settings.',
          buttons: ['OK']
        });
        alert.present();
      });
  }

  /**
   * Is called when a book was selected.
   * Returns the application to the reader page with the selected book as parameter.
   * @param book
   */
  loadText(book: Book) {
    this.books.length = 0;

    console.log("length of nav stack: " + this.navCtrl.length());
    if (this.navCtrl.length() > 1) {
      this.navCtrl.remove(0, this.navCtrl.length() - 1);
    }
    this.navCtrl.insert(0, ReaderPage, {
      bookUrn: book.urn,
      bookTitle: book.title,
      bookAuthor: book.author.name,
      baseUrl: this.navParams.get("baseUrl")
    });
    this.navCtrl.pop();
  }

  /**
   * Opens an alert to display the licence and source information of a book.
   * @param book
   */
  showLicence(book: Book) {
    this.bookService.getBookLicence(book.urn, this.navParams.get("baseUrl")).subscribe((licence: string) => {
      if (licence.length == 0) {
        licence = 'No license information available.';
      }

      this.bookService.getBookSource(book.urn, this.navParams.get("baseUrl")).subscribe((source: string) => {
        if (source.length == 0) {
          source = 'No source information available.';
        }

        let alert = this.alertCtrl.create({
          title: 'License',
          subTitle: licence,
          message: source,
          buttons: ['OK']
        });
        alert.present();
      });
    });
  }
}




