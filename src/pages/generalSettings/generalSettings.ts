import {ViewController, App} from "ionic-angular";
import {Component, DoCheck, OnInit} from "@angular/core";
import {Storage} from "@ionic/storage";
import {SettingsKeys} from "../../util/settingskeys";
import {ReaderPage} from "../reader/reader";

/**
 * Class for the GeneralSettings component.
 * It allows to define the number of urn chunks that are displayed
 * and also provides the ability to clear the local storage.
 */
@Component({
  templateUrl: `generalSettings.html`
})
export class GeneralSettingsPage implements OnInit, DoCheck {

  textLength: number;
  oldNumberUrnChunks: number;

  constructor(public appCtrl: App,
              public viewCtrl: ViewController,
              public storage: Storage) {
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  /**
   * Clears the local storage and switches the application to the ReaderPage.
   */
  clearAppData() {
    this.storage.clear();
    this.appCtrl.getRootNav().setRoot(ReaderPage);
    this.viewCtrl.dismiss();
  }

  ngOnInit() {
    this.storage.get(SettingsKeys.TEXT_LENGTH).then((val) => {
      this.textLength = val;
      this.oldNumberUrnChunks = this.textLength;
    });
  }

  ngDoCheck() {
    if (this.textLength !== this.oldNumberUrnChunks) {
      this.storage.set(SettingsKeys.TEXT_LENGTH, this.textLength);
      this.oldNumberUrnChunks = this.textLength;
    }
  }
}
