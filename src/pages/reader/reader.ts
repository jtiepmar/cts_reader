import {
  Component,
  OnInit,
  animate,
  state,
  style,
  transition,
  trigger,
  ViewChild,
  AnimationTransitionEvent,
  HostListener
} from "@angular/core";
import {
  NavController,
  LoadingController,
  Loading,
  ToastController,
  Content,
  NavParams,
  ModalController
} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {ReaderService} from "../../services/reader.service";
import {Book} from "../../model/book";
import {UrnChunk} from "../../model/urnchunk";
import {GeneralSettingsPage} from "../generalSettings/generalSettings";
import {LibraryAuthorPage} from "../library/library-author";
import {BookService} from "../../services/book.service";
import {AboutPage} from "../about/about";
import {SettingsKeys} from "../../util/settingskeys";
import {SearchPage} from "../search/search";
import {DesignPage} from "../design/design";
import {Author} from "../../model/author";
import {BaseUrlPage} from "../baseUrl/baseUrl";
import {PrintPage} from "../print/print";
import {LibraryBooksPage} from "../library/library-books";

/**
 * The reader page of the applications that acts as the main page.
 */
@Component({
  selector: 'page-reader',
  templateUrl: 'reader.html',
  providers: [ReaderService],

  // transitions for turning the pages forward/backward
  animations: [
    trigger('flyInOut', [
      state('in', style({transform: 'translateX(0)'})),
      transition('* => outRight', [
        animate(300,
          style({opacity: 0, transform: 'translateX(-150%)'})),
        animate(1,
          style({opacity: 0, transform: 'translateX(150%)'})),
        animate(300,
          style({opacity: 1, transform: 'translateX(0%)'})),
      ]),

      transition('* => outLeft', [
        animate(300,
          style({opacity: 0, transform: 'translateX(150%)'})),
        animate(1,
          style({opacity: 0, transform: 'translateX(-150%)'})),
        animate(300,
          style({opacity: 1, transform: 'translateX(0%)'})),
      ]),

    ]),
  ]
})
export class ReaderPage implements OnInit {

  /**
   * Binds the Content so it can be used to scroll to the top on turning pages.
   */
  @ViewChild(Content)
  content: Content;

  /**
   * The currently selected book.
   */
  selectedBook: Book;

  /**
   * Start index of the current urn chunk.
   * @type {number}
   */
  private sectionIndex: number = 0;

  /**
   * End index of the current urn chunk.
   * @type {number}
   */
  private sectionEndIndex: number = 0;

  /**
   * Allowed total text length to display.
   */
  private textLengthSetting: number;

  /**
   * Contains all urn chunks of a book together with their length.
   * @type {any[]}
   */
  private chunkUrns: Array<UrnChunk> = Array<UrnChunk>();

  displayText: string = 'Please select a text from the library.';

  private loading: Loading = null;

  flyInOutState: String = 'in';

  /**
   * Magic numbers that are equal to swiping to the sides.
   * @type {number}
   */
  private SWIPE_LEFT: number = 2;
  private SWIPE_RIGHT: number = 4;

  constructor(private navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              public modalCtrl: ModalController,
              private navParams: NavParams,
              private readerService: ReaderService,
              private bookService: BookService,
              public storage: Storage) {

    // check for settings in storage, if empty set defaults
    this.storage.length().then((val: number) => {
      if (val == 0) {
        console.log('Setting default values');
        this.storage.set(SettingsKeys.TEXT_LENGTH, 1000);

        this.storage.set(SettingsKeys.FONT_SIZE, '18px');
        this.storage.set(SettingsKeys.FONT_FAMILY, 'Georgia, serif');
        this.storage.set(SettingsKeys.LINE_HEIGHT, '150%');
        this.storage.set(SettingsKeys.MARGIN_WIDTH, '8%');
      }

      this.storage.get(SettingsKeys.FONT_SIZE).then((fontSize: string) => {
        this.fontSize = fontSize;
      });

      this.storage.get(SettingsKeys.FONT_FAMILY).then((fontFamily: string) => {
        this.fontFamily = fontFamily;
      });

      this.storage.get(SettingsKeys.LINE_HEIGHT).then((lineHeight: string) => {
        this.lineHeight = lineHeight;
      });

      this.storage.get(SettingsKeys.MARGIN_WIDTH).then((marginWidth: string) => {
        this.marginWidth = marginWidth;
      });
    });
  }

  /**
   * Checks if a base url was provided otherwise open a modal to ask the user for it.
   * If a base url is available a check for a provided book urn is provided.
   * Depending if it is only the base urn or a complete range the book is opened.
   */
  ngOnInit() {

    let baseUrl: string = this.navParams.get('baseUrl');
    if (typeof baseUrl == 'undefined') {

      let baseUrlModal = this.modalCtrl.create(BaseUrlPage);
      baseUrlModal.onDidDismiss(data => {
        baseUrl = data.baseUrl;
        this.navCtrl.setRoot(ReaderPage, {baseUrl: baseUrl});

      });
      baseUrlModal.present();

    } else {
      let selectedBookUrn: string = this.navParams.get('bookUrn');

      if (typeof selectedBookUrn != 'undefined') {

        if (!(selectedBookUrn.trim().slice(-1) == ':')) {
          this.openBookFromGivenPosition(selectedBookUrn)
        } else {
          this.openBookFromLastSavedPosition(selectedBookUrn);
        }
      } else {
        this.openLastBook()
      }
    }
  }

  /**
   * The information about the last viewed book is retrieved from local storage
   * to then reopen the reader page with that information as parameter.
   */
  private openLastBook() {
    this.storage.get(SettingsKeys.LAST_BOOK_URN).then((lastBookUrn: string) => {
      if (lastBookUrn != null) {
        this.storage.get(lastBookUrn).then((completeLastBookUrn: string) => {
          console.log('open last book : ' + completeLastBookUrn);
          this.navCtrl.setRoot(ReaderPage, {
            bookUrn: completeLastBookUrn,
            baseUrl: this.navParams.get("baseUrl")
          });
        });
      }
    });
  }

  /**
   * Opens the book from the given urn. If the end of the urn range is provided
   * it is considered otherwise it will be calculated later.
   * @param selectedBookUrn
   */
  private openBookFromGivenPosition(selectedBookUrn: string) {
    console.log('open book from given position: ' + selectedBookUrn);
    let bookUrn: string = selectedBookUrn.substring(0, selectedBookUrn.lastIndexOf(':') + 1);
    let subUrnStart: string = selectedBookUrn.substring(selectedBookUrn.lastIndexOf(':') + 1, selectedBookUrn.length);
    let subUrnEnd: string = '';
    if (subUrnStart.indexOf('-') != -1) {
      subUrnEnd = subUrnStart.substr(subUrnStart.indexOf('-') + 1, subUrnStart.length);
      subUrnStart = subUrnStart.substr(0, subUrnStart.indexOf('-'));
    }
    this.buildBookToDisplay(bookUrn, subUrnStart, subUrnEnd);
  }

  /**
   * Tries to open the selected book from the last position if that information
   * is available from local storage.
   * @param selectedBookUrn
   */
  private openBookFromLastSavedPosition(selectedBookUrn: string) {
    console.log('open book from last saved position: ' + selectedBookUrn);
    this.storage.get(selectedBookUrn).then((completeLastBookUrn: string) => {
      console.log('last saved complete urn: ' + completeLastBookUrn);

      if (completeLastBookUrn != null) {
        let subUrnStart = completeLastBookUrn.substring(completeLastBookUrn.lastIndexOf(':') + 1, completeLastBookUrn.length);

        if (subUrnStart.indexOf('-') != -1) {
          let subUrnEnd: string = subUrnStart.substr(subUrnStart.indexOf('-') + 1, subUrnStart.length);
          subUrnStart = subUrnStart.substr(0, subUrnStart.indexOf('-'));
          this.buildBookToDisplay(selectedBookUrn, subUrnStart, subUrnEnd);
        }
      } else {
        this.buildBookToDisplay(selectedBookUrn, '', '');
      }
    });
  }

  /**
   * Depending on the source from where this function was called additional
   * information about the book needs to be retrieved before loading and displaying it.
   * @param selectedBookUrn
   * @param subUrnStart
   * @param subUrnEnd
   */
  private buildBookToDisplay(selectedBookUrn: string, subUrnStart: string, subUrnEnd: string) {
    let book = new Book();
    book.urn = selectedBookUrn;
    book.author = new Author();
    book.author.name = this.navParams.get('bookAuthor');
    let bookTitle = this.navParams.get('bookTitle');

    if (typeof book.author.name == 'undefined') {
      this.bookService.getBookAuthor(selectedBookUrn, this.navParams.get("baseUrl")).subscribe((author: string) => {
        book.author.name = author;
        this.prepareBookWithTitle(bookTitle, book, subUrnStart, subUrnEnd);
      });
    } else {
      this.prepareBookWithTitle(bookTitle, book, subUrnStart, subUrnEnd);
    }
  }

  private prepareBookWithTitle(bookTitle: string, book: Book, subUrnStart: string, subUrnEnd: string) {
    if (typeof bookTitle !== 'undefined') {
      book.title = bookTitle;
      this.loadAndDisplayBook(book, subUrnStart, subUrnEnd);
    } else {
      this.bookService.getBookTitle(book.urn, this.navParams.get("baseUrl")).subscribe((title: string) => {
        book.title = title;
        this.loadAndDisplayBook(book, subUrnStart, subUrnEnd);
      });
    }
  }

  /**
   * Fetches all urns of a book with the reader service and assigns the initial range indices.
   * At last the text for the urn range is retrieved and displayed.
   * @param book
   * @param subUrnStart
   * @param subUrnEnd
   */
  private loadAndDisplayBook(book: Book, subUrnStart: string, subUrnEnd: string) {
    this.showLoading();

    this.sectionIndex = 0;
    this.sectionEndIndex = 0;
    this.textLengthSetting = 0;
    this.chunkUrns = Array<UrnChunk>();
    this.selectedBook = book;
    this.displayText = '';

    this.readerService.getChunkUrns(this.selectedBook.urn, this.navParams.get("baseUrl")).subscribe((chunkUrns: Array<UrnChunk>) => {
      this.chunkUrns = chunkUrns;
      console.log('Fetched chunk urns: ' + this.chunkUrns.length);

      this.storage.get(SettingsKeys.TEXT_LENGTH).then((textLength: number) => {
        this.textLengthSetting = textLength;

        // if a urn for start was provided we need to calculate the index from where to start the text
        if (subUrnStart.length > 1) {
          this.sectionIndex = this.findUrnIndex(subUrnStart);
        } else {
          this.sectionIndex = 0;
        }

        // if a urn for end was provided we need to calculate the index where to end the text
        if (subUrnEnd.length > 1) {
          this.sectionEndIndex = this.findUrnIndex(subUrnEnd);
        } else {
          this.sectionEndIndex = this.calculateRangeEnd(this.sectionIndex, this.textLengthSetting);
        }

        if (this.chunkUrns.length > 0) {
          this.updateText(this.sectionIndex, this.sectionEndIndex, this.textLengthSetting);
        } else {
          this.displayText = 'The selected book has no content.'
        }

        this.dismissLoading();
      });
    });
  }

  /**
   * Finds the index of the provided urn in the array of all urns of a book.
   * @param urn
   * @returns {number}
   */
  private findUrnIndex(urn: string): number {
    let urnIndex: number = 0;
    for (let i = 0; i < this.chunkUrns.length; i++) {
      if (this.chunkUrns[i].urn.trim().localeCompare(urn.trim()) == 0) {
        urnIndex = i;
        break;
      }
    }
    return urnIndex;
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  dismissLoading() {
    this.loading.dismiss();
  }


  /**
   * Handles swiping input.
   * @param e
   */
  swipeEvent(e) {
    if (e.direction == this.SWIPE_LEFT) {
      console.log('swipe left');
      this.turnForward();
    } else if (e.direction == this.SWIPE_RIGHT) {
      console.log('swipe right');
      this.turnBack();
    }
  }

  private enableHotKeys: boolean = true;

  /**
   * Disables controlling by keys when the application changes to another page
   * to prevent errors.
   */
  ionViewDidLeave() {
    this.enableHotKeys = false;
  }

  /**
   * Enables controlling by keys when the application returns to the reader page.
   */
  ionViewDidEnter() {
    this.enableHotKeys = true;
  }

  /**
   * Handles key input on the reader page to allow turning pages with the arrow keys.
   * @param event
   */
  @HostListener('window:keydown', ['$event'])
  onKey(event: KeyboardEvent) {
    if (!this.enableHotKeys || this.flyInOutState != 'in') {
      return;
    }
    if (event.key == 'ArrowRight') {
      this.turnForward();
    } else if (event.key == 'ArrowLeft') {
      this.turnBack();
    }
  }

  /**
   * Calculates the new index range when turning back and
   * then calls to update the text with the new range.
   */
  turnBack() {
    this.storage.get(SettingsKeys.TEXT_LENGTH).then((textLength: number) => {
      let urnRangeEnd: number = Number(this.sectionIndex - 1);
      if (urnRangeEnd < 0) {
        this.presentToast('Arrived at start of text.');
        return;
      }
      let urnRangeStart: number = this.calculateRangeStart(urnRangeEnd, textLength);
      if (urnRangeStart == 0) {
        urnRangeEnd = this.calculateRangeEnd(urnRangeStart, textLength);
      }
      this.flyOutLeft();
      this.changePages(urnRangeStart, urnRangeEnd, textLength);
    });
  }

  /**
   * Calculates the new index range when turning forward and
   * then calls to update the text with the new range.
   */
  turnForward() {
    this.storage.get(SettingsKeys.TEXT_LENGTH).then((textLength: number) => {
      let urnRangeStart: number = Number(this.sectionEndIndex + 1);
      if (urnRangeStart >= this.chunkUrns.length) {
        this.presentToast('Arrived at end of text.');
        return;
      }
      let urnRangeEnd = this.calculateRangeEnd(urnRangeStart, textLength);
      this.flyOutRight();
      this.changePages(urnRangeStart, urnRangeEnd, textLength);
    });
  }

  skipBackward() {
    this.storage.get(SettingsKeys.TEXT_LENGTH).then((textLength: number) => {
      let urnRangeStart: number = 0;
      let urnRangeEnd = this.calculateRangeEnd(urnRangeStart, textLength);
      this.flyOutRight();
      this.changePages(urnRangeStart, urnRangeEnd, textLength);
    });
  }

  skipForward() {
    this.storage.get(SettingsKeys.TEXT_LENGTH).then((textLength: number) => {
      let urnRangeEnd: number = this.chunkUrns.length - 1;
      let urnRangeStart: number = this.calculateRangeStart(urnRangeEnd, textLength);
      this.flyOutRight();
      this.changePages(urnRangeStart, urnRangeEnd, textLength);
    });
  }

  /**
   * Updates the text with the urn range and scrolls the view to the top.
   * @param urnRangeStart
   * @param urnRangeEnd
   * @param textLength
   */
  private changePages(urnRangeStart: number, urnRangeEnd: number, textLength: number) {
    this.updateText(urnRangeStart, urnRangeEnd, textLength);
    this.scrollToTop();
  }

  /**
   * Calculates the end of the urn range based on the set text length.
   * @param urnIndex
   * @param textLength
   * @returns {number}
   */
  private calculateRangeEnd(urnIndex: number, textLength: number): number {
    let totalTextLength: number = 0;
    while (urnIndex < this.chunkUrns.length - 1) {
      totalTextLength += Number(this.chunkUrns[urnIndex].length);
      if (totalTextLength >= textLength) {
        break;
      } else {
        urnIndex++;
      }
    }
    return urnIndex;
  }

  /**
   * Calculates the start of the urn range based on the set text length.
   * @param urnRangeStart
   * @param textLength
   * @returns {number}
   */
  private calculateRangeStart(urnRangeStart: number, textLength: number) {
    let totalTextLength: number = 0;
    while (urnRangeStart > 0) {
      totalTextLength += Number(this.chunkUrns[urnRangeStart].length);
      if (totalTextLength >= textLength) {
        break;
      } else {
        urnRangeStart--;
      }
    }
    return urnRangeStart;
  }

  /**
   * Fetches the text of the urn range, displays it, updates the fields with the new urn range
   * information, updates the url to reflect the change.
   * @param urnRangeStart
   * @param urnRangeEnd
   * @param textLength
   */
  private updateText(urnRangeStart: number, urnRangeEnd: number, textLength: number) {
    let urnRange = this.chunkUrns[urnRangeStart].urn + '-' + this.chunkUrns[urnRangeEnd].urn;
    let completeUrn = this.selectedBook.urn + urnRange;
    console.log('Fetching urn: ' + completeUrn);
    this.readerService.getPassage(completeUrn, this.navParams.get("baseUrl"))
      .subscribe(chunkText => {
        this.displayText = ReaderPage.formatText(chunkText);
        this.sectionIndex = urnRangeStart;
        this.sectionEndIndex = urnRangeEnd;
        this.textLengthSetting = textLength;

        history.replaceState(null, '', "#/reader/" + this.navParams.get("baseUrl") + '/' + completeUrn);

        // TODO set bookmark for multiple books
        console.log('setting bookmark for ' + completeUrn);
        this.storage.set(SettingsKeys.LAST_BOOK_URN, this.selectedBook.urn);
        this.storage.set(this.selectedBook.urn, completeUrn);
      });
  }

  private presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 1000,
      position: 'middle'
    });
    toast.present();
  }

  /**
   * Opens the general settings modal.
   * @param myEvent
   */
  openGeneral(myEvent) {
    this.storage.get(SettingsKeys.LAST_AUTHOR).then((author: string) => {
      this.selectedBook.author.name = author;
      let generalSettingsModal = this.modalCtrl.create(GeneralSettingsPage);
      generalSettingsModal.present();
    });
  }

  /**
   * Opens the library page.
   * @param myEvent
   */
  openLibrary(myEvent) {
    this.navCtrl.push(LibraryAuthorPage, {
      baseUrl: this.navParams.get('baseUrl')
    });
  }

  isAuthorDefined(): boolean {
    return typeof this.selectedBook != 'undefined'
    && typeof this.selectedBook.author != 'undefined'
    && typeof this.selectedBook.author.name != 'undefined';
  }

  /**
   * Opens the library page of the current author.
   * @param myEvent
   */
  openLibraryAuthor(myEvent) {
    console.log(this.selectedBook);
    this.navCtrl.push(LibraryBooksPage, {
      baseUrl: this.navParams.get('baseUrl'),
      authorName: this.selectedBook.author.name
    });
  }

  /**
   * Opens the search page.
   * @param myEvent
   */
  openSearch(myEvent) {
    this.navCtrl.push(SearchPage, {
      baseUrl: this.navParams.get('baseUrl')
    });
  }

  /**
   * Opens the about modal.
   * @param myEvent
   */
  openAbout(myEvent) {
    let aboutModal = this.modalCtrl.create(AboutPage, {
      urn: this.selectedBook.urn,
      title: this.selectedBook.title
    });
    aboutModal.present();
  }

  /**
   * Opens the print modal with the current settings
   * and stores any changes to the them when the modal is closed.
   * @param myEvent
   */
  openPrint(myEvent) {
    this.storage.get(SettingsKeys.LAST_AUTHOR).then((author: string) => {
      this.selectedBook.author.name = author;
      this.bookService.getBookLicence(this.selectedBook.urn, this.navParams.get("baseUrl"))
        .subscribe((licence: string) => {
          if (licence.length == 0) {
            licence = 'No license information available.';
          }
          let printModal = this.modalCtrl.create(PrintPage, {
            licence: licence,
            urn: this.selectedBook.urn,
            title: this.selectedBook.title,
            author: this.selectedBook.author.name,
            baseUrl: this.navParams.get('baseUrl')
          });

        printModal.present();
      });
    });
  }

  scrollToTop() {
    this.content.scrollToTop();
  }

  /**
   * Updates the image on the left side of the
   * screen representing the position in the current book.
   * @returns {string}
   */
  setProgressWidthLeft() {
    let max: number = 100;
    if (this.chunkUrns.length > 0) {
      max = Math.ceil((this.sectionIndex / this.chunkUrns.length) * 100);
    }
    if (max == 0) {
      max++;
    }
    return 'linear-gradient(to right, ' + this.buildGradientString(max) + ')';
  }

  /**
   * Updates the image on the right side of the
   * screen representing the position in the current book.
   * @returns {string}
   */
  setProgressWidthRight() {
    let max: number = 100;
    if (this.chunkUrns.length > 0) {
      max = Math.ceil(100 - ((this.sectionEndIndex / this.chunkUrns.length) * 100));
    }
    return 'linear-gradient(to left, ' + this.buildGradientString(max) + ')';
  }

  /**
   * Creates the string that defines the gradient on the sides.
   * @param max
   * @returns {string}
   */
  private buildGradientString(max: number): string {
    if (this.chunkUrns.length == 0) {
      max = 0;
    }
    let subString: string = '';
    let i: number = 0;
    while (i < max) {
      subString += 'rgba(120,120,120,1) ' + i++ + '%,rgba(120,120,120,1) ' + i++ +
        '%,rgba(240,240,240,1) ' + i++ + '%,rgba(240,240,240,1) ' + i++ + '%';
      if (i < max - 1) {
        subString += ','
      }
    }
    return subString;
  }

  /**
   * Helper method to displayed some escaped characters to reduce some noise in the text.
   * @param text
   * @returns {string}
   */
  static formatText(text: string) {
    return text.replace(/(?:&lt;)/g, '<')
      .replace(/(?:&gt;)/g, '>')
      .replace(/(?:&quot;)/g, '"')
  }

  /**
   * Triggers the animation to fly the text out to the right.
   */
  private flyOutRight() {
    this.flyInOutState = 'in';
    this.flyInOutState = 'outRight';
  }

  /**
   * Triggers the animation to fly the text out to the left.
   */
  private flyOutLeft() {
    this.flyInOutState = 'in';
    this.flyInOutState = 'outLeft';
  }

  /**
   * Resets the state back at the end of the animations.
   * @param event
   */
  animationDone(event: AnimationTransitionEvent) {
    this.flyInOutState = 'in';
  }


  fontSize: string;
  fontFamily: string;
  lineHeight: string;
  marginWidth: string;

  /**
   * Opens the design settings modal.
   * @param myEvent
   */
  openDesign(myEvent) {
    let designModal = this.modalCtrl.create(DesignPage, {
      fontSize: this.fontSize,
      fontFamily: this.fontFamily,
      lineHeight: this.lineHeight,
      marginWidth: this.marginWidth
    });

    designModal.onDidDismiss(data => {
      if (data != null) {
        this.fontSize = data.fontSize;
        this.storage.set(SettingsKeys.FONT_SIZE, this.fontSize);

        this.lineHeight = data.lineHeight;
        this.storage.set(SettingsKeys.LINE_HEIGHT, this.lineHeight);

        this.marginWidth = data.marginWidth;
        this.storage.set(SettingsKeys.MARGIN_WIDTH, this.marginWidth);

        this.fontFamily = data.fontFamily;
        this.storage.set(SettingsKeys.FONT_FAMILY, this.fontFamily);
      }
    });

    designModal.present();
  }

  setTextFontSize(): string {
    return this.fontSize;
  }

  setTextFontFamily(): string {
    return this.fontFamily;
  }

  setTextLineHeight(): string {
    return this.lineHeight;
  }

  setMarginWidth(): string {
    return this.marginWidth;
  }

  /**
   * Fixes the view element width to acommodate dynamic changes of the side margins.
   */
  setAntiMarginWidth(): string {
    if (typeof this.marginWidth == "undefined"){
      this.marginWidth = '1%';
    }
    let marginNumber = Number(this.marginWidth.substr(0, this.marginWidth.length - 1));
    let result: number = 100 - 2 * marginNumber;
    return (result.toString() + "%");
  }
}
