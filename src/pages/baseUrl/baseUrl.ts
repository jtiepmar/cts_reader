import {Component, OnInit} from "@angular/core";
import {NavParams, ViewController} from "ionic-angular";

/**
 * Class for the BaseUrl component.
 * It allows to define the base url of the cts server
 * if none is provided in the url.
 */
@Component({
  templateUrl: `baseUrl.html`
})
export class BaseUrlPage implements OnInit {

  baseUrl: string;

  constructor(public navParams: NavParams,
              public viewCtrl: ViewController) {
  }

  ngOnInit() {
    if (typeof this.baseUrl == 'undefined') {
      this.baseUrl = 'cts';
    }
  }

  /**
   * Closes the component and returns the entered base url.
   */
  dismiss() {
    this.viewCtrl.dismiss({baseUrl: this.baseUrl});
  }
}
