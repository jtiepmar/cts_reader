import {Component} from "@angular/core";
import {NavController, AlertController, NavParams} from "ionic-angular";
import {BookService} from "../../services/book.service";
import {Book} from "../../model/book";
import {ReaderPage} from "../reader/reader";

/**
 * Class for the SearchPage component.
 */
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {

  searchQuery: string = '';
  books: Array<Book> = Array<Book>();

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public bookService: BookService) {
  }

  ngOnInit() {

  }

  /**
   * Fetches all books that match the current search query.
   * But requires at least three characters.
   * @param ev
   */
  onInput(ev: any) {
    console.log('searching...');
    if (this.searchQuery.length < 3) {
      return;
    }

    this.bookService.searchBooks(this.searchQuery, this.navParams.get("baseUrl"))
      .subscribe(books => {
        this.books = books
      }, (error) => {
        let alert = this.alertCtrl.create({
          title: 'Error!',
          subTitle: 'There was an error retrieving the list of books. '
          + 'Please check your connection and settings.',
          buttons: ['OK']
        });
        alert.present();
      });
  }

  /**
   * Opens the selected book on the reader page.
   * @param book
   */
  loadText(book: Book) {
    this.books.length = 0;
    this.navCtrl.setRoot(ReaderPage, {
      bookUrn: book.urn,
      bookTitle: book.title,
      bookAuthor: book.author,
      baseUrl: this.navParams.get("baseUrl")
    });
  }
}
