import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Storage} from '@ionic/storage';
import "rxjs/Rx";
import {Observable} from "rxjs";
import {Author} from "../model/author";

/**
 * This service retrieves everything regarding the authors of books.
 */
@Injectable()
export class AuthorService {

  /**
   * Defines the api endpoint on the cts server.
   * @type {string}
   */
  private authorsUrl = 'plain/authors';

  constructor(private http: Http,
              public storage: Storage) {
  }

  /**
   * Retrieves all authors of a cts repository as an Observable.
   * @param baseUrl
   * @returns {Observable<Array<Author>>}
   */
  getAuthors(baseUrl: string): Observable<Array<Author>> {
    return this.http.get('/' + baseUrl + '/' + this.authorsUrl)
      .map(this.extractAuthors)
      .catch(this.handleError);
  }

  /**
   * Standard method to handle errors that could appear during the retrieval of the authors.
   * @param error
   * @returns {any}
   */
  private handleError(error: Response | any) {
    console.error('An error occurred', error);
    return Observable.throw(error);
  }

  /**
   * Helper method to extract the author names from the tab separated response.
   * @param res
   * @returns {Author[]}
   */
  private extractAuthors(res: Response): Array<Author> {
    return res.text()
      .split("\n")
      .map((line) => {
        let author = new Author();
        author.name = line;
        return author;
      }).filter(author => author.name.length > 0 && author.name.indexOf('null') == -1)
  }
}

