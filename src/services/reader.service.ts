import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Storage} from '@ionic/storage';
import "rxjs/add/operator/toPromise";
import {Observable} from "rxjs";
import {UrnChunk} from "../model/urnchunk";

/**
 * This service is used to retrieve the urn chunks of a Book and their text passages.
 */
@Injectable()
export class ReaderService {

  /**
   * Api endpoint for retrieving text passages by its urn.
   * @type {string}
   */
  private getPassageUrl = 'plain/getPassage?urn=';

  /**
   * Api flag to deliver a text passage with new lines.
   * @type {string}
   */
  private getPassageConfiguration = '&configuration=newlines=true';

  /**
   * Api endpoint for all urns with their text length that are below the provided urn.
   * @type {string}
   */
  private urnTextTypesLengthUrl = 'plain/urnstypestextlength?urn=';

  constructor(private http: Http,
              public storage: Storage) {
  }

  /**
   * Standard method to handle errors that could appear during any retrieval.
   * @param error
   * @returns {Promise<never>}
   */
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  /**
   * Fetches the text passage of an urn.
   * @param urn
   * @param baseUrl
   * @returns {Observable<string>}
   */
  getPassage(urn: string, baseUrl: string): Observable<string> {
    let fullUrl = '/' + baseUrl + '/' + this.getPassageUrl + urn + this.getPassageConfiguration;
    return this.http.get(fullUrl)
      .map(response => response.text())
      .catch(this.handleError);
  }

  /**
   * Fetches a list of all urn chunks that are below the book urn.
   * @param bookUrn
   * @param baseUrl
   * @returns {Observable<Array<UrnChunk>>}
   */
  getChunkUrns(bookUrn: string, baseUrl: string): Observable<Array<UrnChunk>> {
    let fullUrl = '/' + baseUrl + '/' + this.urnTextTypesLengthUrl + bookUrn;
    console.log('full url: ' + fullUrl);
    return this.http.get(fullUrl)
      .map(response => {
        let chunkUrns: Array<UrnChunk> = Array<UrnChunk>();
        let lines: Array<string> = response.text().split('\n');
        lines.shift();
        for (let line of lines) {
          let lineSplits = line.split('\t');

          let fullUrn = lineSplits[0];
          let urnParts = fullUrn.split(':');
          let passageIndex = urnParts[urnParts.length - 1];

          let length: number = Number(lineSplits[2]);
          if (!isNaN(length)) {
            chunkUrns.push(new UrnChunk(passageIndex, length));
          }
        }
        return chunkUrns;
      })
      .catch(this.handleError);
  }
}
