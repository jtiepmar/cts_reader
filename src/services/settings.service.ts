import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/Rx';
import {SettingsKeys} from "../util/settingskeys";
import {Storage} from "@ionic/storage";

/**
 * Service to handle themes
 */
@Injectable()
export class SettingsService {

  /**
   * The currently used theme. Bound as a BehaviorSubject so changes to it can be registered.
   * @type {BehaviorSubject}
   */
  private theme: BehaviorSubject<string> = new BehaviorSubject('day-theme');

  /**
   * List of all available themes. A themes is represented
   * by a tuple of the class name used for assignment
   * and its pretty name that is used in the user interface
   */
  availableThemes: {
    className: string,
    prettyName: string
  }[];

  /**
   * Checks if there are any theme settings already in the local storage.
   * If a value is available uses that, or sets the day-theme as default.
   * @param storage
   */
  constructor(public storage: Storage) {
    this.storage.get(SettingsKeys.THEME).then((theme: string) => {
      if (theme != null) {
        console.log('theme from storage: ' + theme);
        this.theme.next(theme);
      } else {
        console.log('setting initial theme');
        this.storage.set(SettingsKeys.THEME, 'day-theme');
      }
    });

    this.availableThemes = [
      {className: 'day-theme', prettyName: 'Day theme'},
      {className: 'night-theme', prettyName: 'Night theme'}
    ];
  }

  /**
   * Changes the theme to the new value and also stores it in local storage.
   * @param val
   */
  setTheme(val) {
    this.storage.set(SettingsKeys.THEME, val);
    this.theme.next(val);
  }

  /**
   * Returns the current theme.
   * @returns {Observable<string>}
   */
  getTheme() {
    return this.theme.asObservable();
  }
}
