import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Storage} from "@ionic/storage";
import "rxjs/Rx";
import {Observable} from "rxjs";
import {Book} from "../model/book";
import {Author} from "../model/author";

/**
 * This service retrieves everything regarding books.
 */
@Injectable()
export class BookService {

  /**
   * Api endpoint for all titles and their urns.
   * @type {string}
   */
  private titlesUrn = 'plain/titlesandurns';

  /**
   * Api endpoint for all titles and their urns with the ability to filter.
   * @type {string}
   */
  private search = 'plain/titlesandurns?filter=';

  /**
   * Api endpoint to retrieve the title of an urn.
   * @type {string}
   */
  private urnMetaTitle = 'plain/metaforkey?key=title&urn=';

  /**
   * Api endpoint to retrieve the licence of an urn.
   * @type {string}
   */
  private urnMetaLicence = 'plain/metaforkey?key=license&urn=';

  /**
   * Api endpoint to retrieve the source of an urn.
   * @type {string}
   */
  private urnMetaSource = 'plain/metaforkey?key=source&urn=';

  /**
   * Api endpoint to retrieve the author of an urn.
   * @type {string}
   */
  private urnMetaAuthor = 'plain/metaforkey?key=author&urn=';

  constructor(private http: Http,
              public storage: Storage) {
  }

  /**
   * Fetches all Books of an Author.
   * @param author
   * @param baseUrl
   * @returns {Observable<Array<Book>>}
   */
  getBooks(author: Author, baseUrl: string): Observable<Array<Book>> {
    let url: string = '/' + baseUrl + '/' +  this.titlesUrn;
    if (author != null && author.name.length > 0) {
      url = url + '?author=' + author.name;
    }
    return this.http.get(url)
      .map(response => this.extractBooks(response, author))
      .catch(this.handleError);
  }

  /**
   * Fetches all Books that match the search query.
   * @param searchQuery
   * @param baseUrl
   * @returns {Observable<Array<Book>>}
   */
  searchBooks(searchQuery: string, baseUrl: string): Observable<Array<Book>> {
    let url: string = '/' + baseUrl + '/' + this.search + searchQuery;
    return this.http.get(url)
      .map(response => this.extractBooks(response, null))
      .catch(this.handleError);
  }

  /**
   * Fetches the title of a Book based on its urn.
   * @param urn
   * @param baseUrl
   * @returns {Observable<string>}
   */
  getBookTitle(urn: string, baseUrl: string): Observable<string> {
    let url: string = '/' + baseUrl + '/' + this.urnMetaTitle + urn;
    return this.http.get(url)
      .map(response => response.text())
      .catch(this.handleError);
  }

  /**
   * Fetches the licence of a Book based on its urn.
   * @param urn
   * @param baseUrl
   * @returns {Observable<string>}
   */
  getBookLicence(urn: string, baseUrl: string): Observable<string> {
    let url: string = '/' + baseUrl + '/' + this.urnMetaLicence + urn;
    return this.http.get(url)
      .map(response => response.text())
      .catch(this.handleError);
  }

  /**
   * Fetches the source of a Book based on its urn.
   * @param urn
   * @param baseUrl
   * @returns {Observable<string>}
   */
  getBookSource(urn: string, baseUrl: string): Observable<string> {
    let url: string = '/' + baseUrl + '/' + this.urnMetaSource + urn;
    return this.http.get(url)
      .map(response => response.text())
      .catch(this.handleError);
  }

  /**
   * Fetches the author of a Book based on its urn.
   * @param urn
   * @param baseUrl
   * @returns {Observable<string>}
   */
  getBookAuthor(urn: string, baseUrl: string): Observable<string> {
    let url: string = '/' + baseUrl + '/' + this.urnMetaAuthor + urn;
    return this.http.get(url)
      .map(response => response.text())
      .catch(this.handleError);
  }

  /**
   * Standard method to handle errors that could appear during any retrieval.
   * @param error
   * @returns {any}
   */
  private handleError(error: Response | any) {
    console.error('An error occurred', error);
    return Observable.throw(error);
  }

  /**
   * Helper method to extract the Books from the tab separated response.
   * @param res
   * @param author
   * @returns {Book[]}
   */
  private extractBooks(res: Response, author: Author): Array<Book> {
    return res.text()
      .split("\n")
      .map(line => {
        let book = new Book();
        let splitLine: Array<string> = line.split("_#_");
        book.title = splitLine[0];
        book.urn = splitLine[1];

        if (author != null) {
          book.author = author;
        }

        return book;
      }).filter(book => book.title.length > 0);
  }
}

