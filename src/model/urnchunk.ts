/**
 * This class holds a urn chunk with its length.
 */
export class UrnChunk {
  constructor(public urn: string, public length: number) {
  }
}
