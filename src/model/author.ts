/**
 * This class reprents an author with its name.
 */
export class Author {
  /**
   * Name of the author.
   */
  name: string;
}
