import {Author} from "./author";

/**
 * This class represents a book.
 */
export class Book {
  /**
   * Title of the book.
   */
  title: string;

  /**
   * Author of the book.
   */
  author: Author;

  /**
   * Base urn of the book.
   */
  urn: string;
}
