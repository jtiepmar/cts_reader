# CTS Reader

Reader for CTS instances based on ionic v2.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The project requires Node.js with npm and ionic.
When Node.js with npm is available install ionic with:
```
npm install -g ionic
```

### Installing

Clone the repository

```
git clone https://heldbrendel@bitbucket.org/heldbrendel/cts_reader.git
```

Fetch the dependencies

```
npm install
```

Start the server

```
npm run ionic:serve
```

## Deployment

To build a deployable version run

```
npm run ionic:build
```

When finished, the necessary files will be located under ```www```.
They then can be deployed on the same server as the CTS instance.

## Theming

Read more about [customizing the look and feel](https://ionicframework.com/docs/v2/theming/) of ionic applications.

## Documentation

The documentation for the project is generated from the JSDOC in the code using compodoc.To update the documentation subdirectory do
```
compodoc -p ./tsconfig.json
```
You can then start via
```
compodoc -s --port 8090
```
and watch the result in your browser.
## Built With

* [Ionic](http://ionicframework.com/docs/) - The web framework used
* [pdfmake](http://pdfmake.org/#/) - Library for generation of PDF files
* [compodoc](https://github.com/compodoc/compodoc) - Documentation tool for Angular projects

## Authors

* **Chris Becker**
* **Marius Wagner**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
